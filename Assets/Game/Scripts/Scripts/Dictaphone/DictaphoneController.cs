﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using Vuforia;
using UnityEngine.UI;



[RequireComponent(typeof(AudioSource))]
public class DictaphoneController : MonoBehaviour {

    public Text PanelSave;

    public VideoPlayer _videoPlayer;
    private AudioSource _audioSource;
    private AudioSource DictaphoneAudioSource
    {
        get
        {
            if (_audioSource == null) return _audioSource = GetComponent<AudioSource>();
            else return _audioSource;
        }
      
    }

    private AudioClip _myAudioClip;
  
    private int _frequency;
    private int Frequency
    {
        get
        {
            var devices = Microphone.devices;
            var minFreg = 0; var maxFreg = 0;

            Microphone.GetDeviceCaps(devices[0], out minFreg, out maxFreg);

            return maxFreg;
        }
    }


    private void Start()
    {
        Record();
        _videoPlayer.loopPointReached += VideoFinished;
        _videoPlayer.Play();

    }
    private bool VidioIsFinished = false;
    private void  VideoFinished(UnityEngine.Video.VideoPlayer vp)
    {
        print("VideoFinishedVideoFinished");
        VidioIsFinished = true;
        PanelSave.gameObject.SetActive(true);
    }

    public void Record()
    {
        Debug.Log("Record");
        _myAudioClip = Microphone.Start(null, false, (int)_videoPlayer.clip.length, Frequency); // false
    }

    public void SaveClip()
    {
        Debug.Log("Save");
        SavWav.Save("myfile", _myAudioClip); // saved in Application.persistentDataPath
        Debug.Log("audio =" + Application.persistentDataPath);
    }

    public void Play()
    {
        Debug.Log("Play");
        DictaphoneAudioSource.clip = _myAudioClip;
        DictaphoneAudioSource.Play();
    }

    private void OnEnable()
    {
        DefaultTrackableEventHandler.YES += YES;
        DefaultTrackableEventHandler.YES += NO;   

    }

    private void OnDisable()
    {
        DefaultTrackableEventHandler.YES -= YES;
        DefaultTrackableEventHandler.YES -= NO;   
    }
    private void YES()
    {
        if (VidioIsFinished)
        {
            SaveClip();
            PanelSave.text = "YES";
        }
    }

    private void NO()
    {
        PanelSave.text = "NO";
    }
}
