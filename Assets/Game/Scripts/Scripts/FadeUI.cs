﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeUI : MonoBehaviour 
{
    public float TimeBeforeFadeUI = 5;
    private Image[] images;
    private Text[] texts;
    private GameObject heartMonitor;// not UI element

    private void Start () 
    {
        images = GetComponentsInChildren<Image>(true); 
        texts =GetComponentsInChildren<Text>(true); 
    //    heartMonitor = GameObject.FindGameObjectWithTag("Heart");
        heartMonitor = GameObject.FindObjectOfType<SWP_HeartRateMonitor>().gameObject;
	}
	
    private void Update () 
    {
        TimeBeforeFadeUI -= Time.deltaTime;

        if (TimeBeforeFadeUI < 0)
        {
            Fade();
        }
	}

    private void Fade()
    {
        for (int i = 0; i < images.Length; i++)
        {
            images[i].color = new Color(images[i].color.r,images[i].color.b,images[i].color.g,images[i].color.a - Time.deltaTime);
        }

        for (int i = 0; i < texts.Length; i++)
        {
            texts[i].color = new Color(texts[i].color.r,texts[i].color.b,texts[i].color.g,texts[i].color.a - Time.deltaTime);
        }
        heartMonitor.SetActive(false);

    }
}
