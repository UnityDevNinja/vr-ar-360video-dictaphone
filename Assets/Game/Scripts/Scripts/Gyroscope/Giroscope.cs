﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Giroscope : MonoBehaviour {

    public GameObject MyCamera;

    void Start () 
    {
        Input.gyro.enabled = true;
    }

    void Update () {
//        if(UIVrController.IFVR())
            MyCamera.transform.Rotate (-Input.gyro.rotationRateUnbiased.x, -Input.gyro.rotationRateUnbiased.y, 0);

    }
}
