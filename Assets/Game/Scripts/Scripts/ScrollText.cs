﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollText : MonoBehaviour {

    [SerializeField]
    private Transform TextTransform;
    [SerializeField]
    private float endPosition;
    [SerializeField]
    private float textSpeed;
	
    private void Update ()
    {
        TextTransform.localPosition = Vector3.MoveTowards(TextTransform.localPosition, new Vector3(TextTransform.localPosition.x, endPosition,TextTransform.localPosition.z), textSpeed);
	}
}
