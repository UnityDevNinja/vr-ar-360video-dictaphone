﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchMaterial : MonoBehaviour 
{
    private MeshRenderer _meshRenderer;
    [SerializeField]
    private Material videoMaterial;  


    private IEnumerator Start()
    {
        var m = new Material[1]{videoMaterial};

        yield return null;
        _meshRenderer = GetComponentInChildren<MeshRenderer>();
        if (_meshRenderer == null)
            print("== null");

        _meshRenderer.materials = m;
    }

}
