﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VR;
using Vuforia;

public class SwitcherVRMode : MonoBehaviour 
{
    public enum VrType{ VR , nonVR }
    private static VrType VRType = VrType.VR;

    public static SwitcherVRMode i;
    [SerializeField]
    private GazeInputModule GIM;


    [SerializeField]
    private GameObject Canvas3D;
    [SerializeField]
    private GameObject Canvas2D;
    [SerializeField]
    private GameObject duoCamera;
    [SerializeField]
    private GameObject SoloCamera;

    public static VrType GetVrType()
    {
        return VRType;
    }

    private void Start()
    {
        i = this;
    }

    public void OnSwitchToVRButton()
    {
        StartCoroutine(SwitchToVR());
    }

    private IEnumerator SwitchToVR()
    {
        print("tovr");
        VRType = VrType.VR;
        GIM.enabled = true;
        SoloCamera.SetActive(false);
        duoCamera.SetActive(true);
        yield return null;

        Canvas3D.SetActive(true);
        Canvas2D.SetActive(false);

        yield return null;
        VRSettings.enabled = true;
    }


    public void OnSwitchOutOfVRButton()
    {
        StartCoroutine(SwitchOutOfVR());
    }

    private IEnumerator SwitchOutOfVR()
    {
        print("outvr");
        VRType = VrType.nonVR;
        GIM.enabled = false;
        SoloCamera.SetActive(true);
        duoCamera.SetActive(false);
        yield return null;

        Canvas3D.SetActive(false);
        Canvas2D.SetActive(true);

        yield return null;
        VRSettings.enabled = false; 
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            OnSwitchOutOfVRButton();
        }
        if (Input.GetKeyDown(KeyCode.B))
        {
            OnSwitchToVRButton();
        }

//        print(VRType);
    }

    private void OnEnable()
    {
        DefaultTrackableEventHandler.FoundTracableObject += DefaultTrackableEventHandler_FoundTracableObject;   
    }

    void DefaultTrackableEventHandler_FoundTracableObject ()
    {
        if (VRType == VrType.nonVR)
            OnSwitchToVRButton();
        else if (VRType == VrType.VR)
            OnSwitchOutOfVRButton();
    }

    private void OnDisable()
    {
        DefaultTrackableEventHandler.FoundTracableObject -= DefaultTrackableEventHandler_FoundTracableObject;   
    }
}
