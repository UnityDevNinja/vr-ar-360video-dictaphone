﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Timer : MonoBehaviour 
{
    public static event Action<float> OnCurrentTimerValue;

    private float _currentTime = 240;

    private void Update()
    {
        _currentTime -= Time.deltaTime;

        if (_currentTime > 0)
        {
            ConvetToClockFormat(_currentTime);
        }
    }

    private void ConvetToClockFormat(float currentTime)
    {
        if (OnCurrentTimerValue != null)
            OnCurrentTimerValue(currentTime);
    }
}