﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerUI : MonoBehaviour 
{
    private Image _image;

    private void Start()
    {
        _image = GetComponent<Image>();
    }

    private void OnEnable()
    {
        Timer.OnCurrentTimerValue += Timer_OnCurrentTimerValue;
    }

    private void OnDisable()
    {
        Timer.OnCurrentTimerValue += Timer_OnCurrentTimerValue;
    }

    private void Timer_OnCurrentTimerValue (float obj)
    {
        _image.fillAmount = 1 - (1 / obj);
    }

}
