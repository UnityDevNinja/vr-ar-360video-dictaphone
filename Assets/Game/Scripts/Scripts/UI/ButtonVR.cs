﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System;
using UnityEngine.UI;

public class ButtonVR : MonoBehaviour {


    private float timer = 0;
    private float gazeTime = 2f;

    private bool gazedAt;

    private void Update()
    {
        if (gazedAt)
        {
            timer += Time.deltaTime;
            if(timer >= gazeTime)
            {
                ExecuteEvents.Execute(gameObject, new PointerEventData(EventSystem.current), ExecuteEvents.pointerDownHandler);
                timer = 0f;
                gazedAt = false;
            }
        }
    }

    public void PointerEnter()
    {
        Debug.Log("PointerEnter");
        gazedAt = true;
    }

    public void PointerExit()
    {
        Debug.Log("PointerExit");
        gazedAt = false;
    }

    public void PointerDown()
    {
        Debug.Log("PointerDown");
        SwitcherVRMode.i.OnSwitchOutOfVRButton();
    }
}
