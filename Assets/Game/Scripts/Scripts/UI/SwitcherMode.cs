﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VR;

public class SwitcherMode : MonoBehaviour {

    public enum VrType { VR,noneVR }
    private static VrType VRType = VrType.VR;

    [SerializeField]
    private GameObject Canvas3D;
    [SerializeField]
    private GameObject Canvas2D;


    [SerializeField]
    private Transform TextTransform;
    private float TextSpeed;
    private float yMax = 0;
    private float endposition = 504;

    float timer = 8;


    public static bool IFVR()
    {
        return VRType == VrType.VR; 
    }


    public void CardBoardButton()// button
    {
        print("CardBoardButton");
        switch (VRType)
        {
            case VrType.VR:// vihod iz vr
                StartCoroutine( SwitchOutOfVR());
                VRType = VrType.noneVR;
                break;
            case VrType.noneVR: // vhod v vr
                StartCoroutine( SwitchToVR());
                VRType = VrType.VR;
                break;
            default:
                break;
        }
    }


    private IEnumerator SwitchToVR()
    {
        VRSettings.LoadDeviceByName("newDevice");

        while (!VRSettings.loadedDeviceName.Equals("newDevice"))
        {
            yield return null;
        }

        for (int i = 0; i < 5; i++)
        {
            yield return null;
        }

        VRSettings.enabled = true;
    }

    private IEnumerator SwitchOutOfVR()
    {
        VRSettings.LoadDeviceByName(""); 
        yield return null;
        VRSettings.enabled = false; 
    }
}
